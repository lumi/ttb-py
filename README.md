# ttb-py

## What's this?

A Python library for writing tinytown .ttb files.

## What license is it under?

GNU AGPLv3 or later
