import struct

TTB_HEADER_A = struct.Struct(">3sBB")
TTB_HEADER_B = struct.Struct(">H")
TTB_HEADER_C = struct.Struct(">B")
TTB_HEADER_PALETTE = struct.Struct(">BBBB")
TTB_BLOCK_HEADER = struct.Struct(">BB")
TTB_BLOCK_BRICK_TYPE = struct.Struct(">B")
TTB_BLOCK_BRICK_DATA = struct.Struct(">HlllBB")

class Writer:
    def __init__(self, inner):
        self.inner = inner
        self.brick_types = []
        self.bricks = []

    def write_header(self, build_name, build_description, palette):
        build_name_bytes = build_name.encode("utf8")
        build_description_bytes = build_description.encode("utf8")
        self.inner.write(TTB_HEADER_A.pack(b"ttb", 1, len(build_name_bytes)))
        self.inner.write(build_name_bytes)
        self.inner.write(TTB_HEADER_B.pack(len(build_description_bytes)))
        self.inner.write(build_description_bytes)
        self.inner.write(TTB_HEADER_C.pack(len(palette)))
        for (r, g, b, a) in palette:
            self.inner.write(TTB_HEADER_PALETTE.pack(r, g, b, a))

    def write_brick_type(self, brick_type):
        if len(self.brick_types) >= 255:
            self._flush_brick_types()
        if brick_type in self.brick_types:
            return self.brick_types.find(brick_type)
        else:
            idx = len(self.brick_types)
            self.brick_types.append(brick_type)
            return idx

    def write_brick(self, brick_type_id, x, y, z, orientation, color_id):
        if len(self.bricks) >= 255:
            self._flush_bricks()
        self.bricks.append((brick_type_id, x, y, z, orientation, color_id))

    def _flush_brick_types(self):
        if len(self.brick_types) > 0:
            self.inner.write(TTB_BLOCK_HEADER.pack(1, len(self.brick_types)))
            for brick_type in self.brick_types:
                brick_type_bytes = brick_type.encode("utf8")
                self.inner.write(TTB_BLOCK_BRICK_TYPE.pack(len(brick_type_bytes)))
                self.inner.write(brick_type_bytes)
            self.brick_types = []

    def _flush_bricks(self):
        if len(self.bricks) > 0:
            if len(self.brick_types) > 0:
                self._flush_brick_types()
            self.inner.write(TTB_BLOCK_HEADER.pack(2, len(self.bricks)))
            for (brick_type_id, x, y, z, orientation, color_id) in self.bricks:
                self.inner.write(TTB_BLOCK_BRICK_DATA.pack(brick_type_id, x, y, z, orientation, color_id))
            self.bricks = []

    def finish(self):
        self._flush_brick_types()
        self._flush_bricks()
